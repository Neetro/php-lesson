<?php

	require_once('functiouns.php');


	$navigation = [
		'#main' => 'Главная',
		'#protein' => 'Протеин',
		'#bcaa' => 'BCAA',
		'#peptide' => 'Пептиды',
		'#contacts' => 'Контакти',
	];
?>
<!DOCTYPE html>
<html>
<head>
	<title>Green Friday</title>
	<style type="text/css">
		nav {
			display: flex;
		}
		nav div {padding: 10px;}
		nav div a {color: green;}
		nav div a:hover {text-decoration: none;}
	</style>
</head>
<body>
	<nav>
		<?php
			showNav($navigation);
		?>
	</nav>

</body>
</html>